package version

import "fmt"

type Version [3]uint

func (ver *Version) String() string {
	return fmt.Sprintf("%d.%d.%d", ver[0], ver[1], ver[2])
}
