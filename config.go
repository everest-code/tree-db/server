package main

import "gitlab.com/everest-code/tree-db/server/log"

type ConfigArgs struct {
	Expose     *string
	LogLevel   *string
	EnableUser *bool
	LogFile    *string
	DataDir    *string
}

func (conf *ConfigArgs) GetLogLevel() log.Level {
	switch *conf.LogLevel {
	case "DEBG":
		return log.LevelDebg
	case "INFO":
		return log.LevelInfo
	case "WARN":
		return log.LevelWarn
	case "ERRO":
		fallthrough
	default:
		return log.LevelErro
	}
}
