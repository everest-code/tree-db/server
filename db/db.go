package db

import (
	"errors"
	"os"
	"strings"

	"gitlab.com/everest-code/tree-db/server/log"
)

type Database struct {
	logger *log.Logger
	Folder string
}

var (
	NotFound      error = errors.New("Node not found")
	AlreadyExists error = errors.New("Node already exists")
)

func New(logger *log.Logger, dbFolder string) *Database {
	return &Database{
		logger: logger,
		Folder: dbFolder,
	}
}

func (db *Database) InitDB() (err error) {
	alreadyExists := false
	err = os.Mkdir(db.Folder, 0o0755)
	if err != nil {
		if os.IsExist(err) {
			alreadyExists = true
			db.logger.Warn("Database already exists " +
				"omitting initialization")
		} else {
			return
		}
	}

	if !alreadyExists {
		db.logger.Info("Creating database structure")
		for _, folder := range []string{"Users", "Nodes", "Roots"} {
			folder = strings.Join([]string{db.Folder, folder}, string(os.PathSeparator))
			err = os.Mkdir(folder, 0o0755)
			if err != nil && !os.IsExist(err) {
				return
			}
		}
	}

	return nil
}

func (db *Database) Users() *UserHandler {
	return &UserHandler{db}
}

func (db *Database) Nodes() *NodeHandler {
	return &NodeHandler{db}
}

func (db *Database) Roots() *RootHandler {
	return &RootHandler{db, db.Nodes()}
}
