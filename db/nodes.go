package db

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type (
	NodeHandler struct {
		database *Database
	}

	Node struct {
		ID        uuid.UUID   `json:"id"`
		Parent    *uuid.UUID  `json:"parent,omitempty"`
		Name      string      `json:"name"`
		CreatedAt time.Time   `json:"creationDate"`
		Type      NodeType    `json:"type"`
		Value     interface{} `json:"value"`
	}
)

var (
	InvalidType error = errors.New("Node has an invalid type for this action")
	CantParse   error = errors.New("The value of the node is not parseable")
)

func getDefaultNodeValue(ntype NodeType) interface{} {
	switch ntype {
	case TNode:
		fallthrough
	case TRoot:
		return make([]uuid.UUID, 0)
	case TList:
		return make([]interface{}, 0)
	case TNumber:
		return 0
	case TStr:
		return ""
	default:
		return nil
	}
}

func (handler *NodeHandler) GetFolder() string {
	return strings.Join([]string{
		handler.database.Folder,
		"Nodes",
	}, string(os.PathSeparator))
}

func (handler *NodeHandler) GetFile(filename string) string {
	return strings.Join([]string{
		handler.GetFolder(),
		fmt.Sprintf("%s.node.json", filename),
	}, string(os.PathSeparator))
}

func (handler *NodeHandler) Get(nodeId uuid.UUID) (node *Node, err error) {
	node = new(Node)
	err = utils.ReadJSON(node, handler.GetFile(nodeId.String()))
	if err != nil {
		if os.IsNotExist(err) {
			err = NotFound
		}
		node = nil
	}

	return
}

func (handler *NodeHandler) RootExistsByName(nodeName string) (exists bool, err error) {
	files, err := utils.ListDir(handler.GetFolder(), true)
	if err != nil {
		return false, err
	}

	for _, file := range files {
		node := new(Node)
		err := utils.ReadJSON(node, file)
		if err != nil {
			return false, err
		}

		if node.Parent == nil && node.Name == nodeName {
			return true, nil
		}
	}

	return false, nil
}

func (handler *NodeHandler) NodeAddChildren(nodeId, child uuid.UUID) error {
	node, err := handler.Get(nodeId)
	if err != nil {
		return err
	}

	if node.Type != TRoot && node.Type != TNode {
		return InvalidType
	}

	_, ok := node.Value.([]interface{})
	if !ok {
		return CantParse
	}

	node.Value = append(node.Value.([]interface{}), child.String())

	return utils.WriteJSON(node, handler.GetFile(node.ID.String()))
}

func (handler *NodeHandler) Exists(nodeId uuid.UUID) (exists bool, err error) {
	_, err = os.Stat(handler.GetFile(nodeId.String()))

	if err == nil {
		exists = true
	} else if os.IsNotExist(err) {
		exists = false
		err = nil
	}

	return
}

func (handler *NodeHandler) HasChildren(nodeId uuid.UUID, childrenName string) (bool, error) {
	if ok, err := handler.Exists(nodeId); err != nil {
		return false, err
	} else if !ok {
		return false, NotFound
	}

	node, err := handler.Get(nodeId)
	if err != nil {
		return false, err
	}

	if node.Type != TRoot && node.Type != TNode {
		return false, InvalidType
	}

	nodeChildren, ok := node.Value.([]interface{})
	if !ok {
		return false, CantParse
	}

	for _, childStr := range nodeChildren {
		child, err := uuid.Parse(childStr.(string))
		if err != nil {
			return false, err
		}

		childNode, err := handler.Get(child)
		if err != nil {
			return false, err
		} else if childNode.Name == childrenName {
			return true, nil
		}
	}

	return false, nil
}

func (handler *NodeHandler) CreateBlank(name string, ntype NodeType, parent *uuid.UUID) (*Node, error) {
	node := &Node{
		ID:        uuid.New(),
		Parent:    parent,
		Name:      name,
		CreatedAt: time.Now(),
		Type:      ntype,
		Value:     getDefaultNodeValue(ntype),
	}

	if node.Name == "<auto>" {
		node.Name = node.ID.String()
	}

	if node.Parent != nil {
		if ok, err := handler.Exists(*node.Parent); err != nil {
			return nil, err
		} else if !ok {
			return nil, NotFound
		}

		if ok, err := handler.HasChildren(*node.Parent, node.Name); err != nil {
			return nil, err
		} else if ok {
			return nil, AlreadyExists
		}
	} else {
		if ok, err := handler.RootExistsByName(node.Name); err != nil {
			return nil, err
		} else if ok {
			return nil, AlreadyExists
		}
	}

	err := utils.WriteJSON(node, handler.GetFile(node.ID.String()))
	if err != nil {
		return nil, err
	}

	if parent != nil {
		if err := handler.NodeAddChildren(*node.Parent, node.ID); err != nil {
			return nil, err
		}
	}

	return node, nil
}

func (handler *NodeHandler) Delete(nodeId uuid.UUID) error {
	node, err := handler.Get(nodeId)
	if err != nil {
		return err
	}

	if node.Parent != nil {
		parentNode, err := handler.Get(*node.Parent)
		if err != nil {
			return err
		}

		arr, err := utils.ToUUIDArray(parentNode.Value)
		nArr := make([]uuid.UUID, 0)
		if err != nil {
			return err
		}

		for _, val := range arr {
			if node.ID.String() != val.String() {
				nArr = append(nArr, val)
			}
		}

		err = handler.UpdateValue(*node.Parent, nArr)
		if err != nil {
			return err
		}
	}

	if node.Type == TNode || node.Type == TRoot {
		for _, val := range node.Value.([]interface{}) {
			id, err := uuid.Parse(val.(string))
			if err != nil {
				return err
			}

			err = handler.Delete(id)
			if err != nil {
				return err
			}
		}
	}

	return os.Remove(handler.GetFile(nodeId.String()))
}

func (handler *NodeHandler) UpdateValue(nodeId uuid.UUID, value interface{}) error {
	node, err := handler.Get(nodeId)
	if err != nil {
		return err
	}

	node.Value = value
	return utils.WriteJSON(node, handler.GetFile(node.ID.String()))
}

func (handler *NodeHandler) Dump(nodeId uuid.UUID) (string, interface{}, error) {
	node, err := handler.Get(nodeId)
	if err != nil {
		return "", nil, err
	}

	if node.Type == TNode {
		retMap := make(map[string]interface{})
		children, ok := node.Value.([]interface{})
		if !ok {
			return "", nil, CantParse
		}

		for _, child := range children {
			id, err := uuid.Parse(child.(string))
			if err != nil {
				return "", nil, err
			}

			k, v, err := handler.Dump(id)
			if err != nil {
				return "", nil, err
			}

			retMap[k] = v
		}

		return node.Name, retMap, nil
	}

	return node.Name, node.Value, nil
}

func (handler *NodeHandler) FindFork(path []string, parentNodes []uuid.UUID) (*Node, error) {
	nameSearch := path[0]
	for _, id := range parentNodes {
		node, err := handler.Get(id)
		if err != nil {
			return nil, err
		}

		if node.Name == nameSearch {
			if len(path) == 1 {
				return node, nil
			} else {
				if node.Type != TNode && node.Type != TRoot {
					return nil, InvalidType
				}

				nodeList := make([]uuid.UUID, 0)
				for _, val := range node.Value.([]interface{}) {
					id, err = uuid.Parse(val.(string))
					if err != nil {
						return nil, err
					}

					nodeList = append(nodeList, id)
				}

				return handler.FindFork(path[1:], nodeList)
			}
		}
	}

	return nil, NotFound
}
