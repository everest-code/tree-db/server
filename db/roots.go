package db

import (
	"fmt"
	"os"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type (
	RootHandler struct {
		database *Database
		nodes    *NodeHandler
	}
)

func (handler *RootHandler) GetFolder() string {
	return strings.Join([]string{
		handler.database.Folder,
		"Roots",
	}, string(os.PathSeparator))
}

func (handler *RootHandler) GetFile(filename string) string {
	return strings.Join([]string{
		handler.GetFolder(),
		fmt.Sprintf("%s.root.json", filename),
	}, string(os.PathSeparator))
}

func (handler *RootHandler) GetRelFile(filename string) string {
	return strings.Join([]string{
		"..",
		"Nodes",
		fmt.Sprintf("%s.node.json", filename),
	}, string(os.PathSeparator))
}

func (handler *RootHandler) Exists(name string) (exists bool, err error) {
	_, err = os.Stat(handler.GetFile(name))

	if err == nil {
		exists = true
	} else if os.IsNotExist(err) {
		exists = false
		err = nil
	}

	return
}

func (handler *RootHandler) Create(name string) (*Node, error) {
	exists, err := handler.Exists(name)
	if err != nil {
		return nil, err
	} else if exists {
		return nil, AlreadyExists
	}

	node, err := handler.nodes.CreateBlank(name, TRoot, nil)
	if node != nil {
		os.Symlink(
			handler.GetRelFile(node.ID.String()),
			handler.GetFile(name),
		)
	}
	return node, err
}

func (handler *RootHandler) List() (nodes []Node, err error) {
	nodes = make([]Node, 0)
	nodeFiles, err := utils.ListDir(handler.GetFolder(), true)
	if err != nil {
		return
	}

	for _, fileName := range nodeFiles {
		node := new(Node)
		err = utils.ReadJSON(node, fileName)
		if err != nil {
			return nil, err
		}

		nodes = append(nodes, *node)
	}

	return
}

func (handler *RootHandler) Get(name string) (node *Node, err error) {
	node = new(Node)
	err = utils.ReadJSON(node, handler.GetFile(name))
	if err != nil {
		if os.IsNotExist(err) {
			err = NotFound
		}
		node = nil
	}

	return
}

func (handler *RootHandler) Delete(name string) (err error) {
	node, err := handler.Get(name)
	if err != nil {
		return err
	}

	if err := handler.nodes.Delete(node.ID); err != nil {
		return err
	}

	return os.Remove(handler.GetFile(name))
}

func (handler *RootHandler) DumpTree(name string) (map[string]interface{}, error) {
	if ok, err := handler.Exists(name); err != nil {
		return nil, err
	} else if !ok {
		return nil, NotFound
	}

	node, err := handler.Get(name)
	if err != nil {
		return nil, err
	}

	children, ok := node.Value.([]interface{})
	if !ok {
		return nil, CantParse
	}
	ret := make(map[string]interface{})
	for _, child := range children {
		id, err := uuid.Parse(child.(string))
		if err != nil {
			return nil, err
		}

		k, v, err := handler.nodes.Dump(id)
		if err != nil {
			return nil, err
		}

		ret[k] = v
	}

	return ret, nil
}

func (handler *RootHandler) FindFork(name string, path []string) (*Node, error) {
	if ok, err := handler.Exists(name); err != nil {
		return nil, err
	} else if !ok {
		return nil, NotFound
	}

	node, err := handler.Get(name)
	if err != nil {
		return nil, err
	}

	if len(path) == 0 {
		return node, nil
	}

	nodeList := make([]uuid.UUID, 0)
	for _, val := range node.Value.([]interface{}) {
		id, err := uuid.Parse(val.(string))
		if err != nil {
			return nil, err
		}

		nodeList = append(nodeList, id)
	}

	return handler.nodes.FindFork(path, nodeList)
}
