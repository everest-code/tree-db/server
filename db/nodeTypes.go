package db

import (
	"bytes"
	"encoding/json"
)

type NodeType uint8

const (
	TNode NodeType = iota
	TRoot
	TBool
	TList
	TStr
	TNumber
	TUnknown
)

var (
	ntToString = map[NodeType]string{
		TNode:   "Node",
		TRoot:   "Root",
		TBool:   "Bool",
		TList:   "List",
		TStr:    "Str",
		TNumber: "Number",
	}

	ntToId = map[string]NodeType{
		"Node":   TNode,
		"Root":   TRoot,
		"Bool":   TBool,
		"List":   TList,
		"Str":    TStr,
		"Number": TNumber,
	}
)

func (nodeType NodeType) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(ntToString[nodeType])
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

func (nodeType *NodeType) UnmarshalJSON(b []byte) error {
	j := new(string)
	if err := json.Unmarshal(b, j); err != nil {
		return err
	}

	var ok bool
	*nodeType, ok = ntToId[*j]
	if !ok {
		*nodeType = TUnknown
	}
	return nil
}

func (nodeType *NodeType) IsValid(value interface{}) bool {
	if *nodeType == TUnknown || *nodeType == TRoot || *nodeType == TNode {
		return false
	}

	var ok bool
	switch *nodeType {
	case TList:
		_, ok = value.([]interface{})
	case TStr:
		_, ok = value.(string)
	case TNumber:
		_, ok = value.(float64)
		if !ok {
			_, ok = value.(int64)
		}
	case TBool:
		_, ok = value.(bool)
	}

	return ok
}
