package db

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type (
	UserHandler struct {
		database *Database
	}

	User struct {
		ID         uuid.UUID `json:"id"`
		Name       string    `json:"name"`
		Password   []uint8   `json:"password,omitempty"`
		CreatedAt  time.Time `json:"creationDate"`
		LastUpdate time.Time `json:"updateDate"`
		CanWrite   bool      `json:"writeAllowed"`
	}
)

func (user *User) Permission() string {
	if user.CanWrite {
		return "rw"
	}

	return "ro"
}

func (handler *UserHandler) GetFolder() string {
	return strings.Join([]string{
		handler.database.Folder,
		"Users",
	}, string(os.PathSeparator))
}

func (handler *UserHandler) GetFile(filename string) string {
	return strings.Join([]string{
		handler.GetFolder(),
		fmt.Sprintf("%s.user.json", filename),
	}, string(os.PathSeparator))
}

func (handler *UserHandler) Count() (int, error) {
	users, err := utils.ListDir(handler.GetFolder(), true)
	if err != nil {
		return -1, err
	}

	handler.database.logger.Debug("User list: %v", users)

	return len(users), nil
}

func (handler *UserHandler) CreateOrUpdate(user *User) error {
	if user.ID == *new(uuid.UUID) {
		user.ID = uuid.New()
	}

	if user.CreatedAt.Equal(*new(time.Time)) {
		user.CreatedAt = time.Now()
		user.LastUpdate = *new(time.Time)
	} else {
		user.LastUpdate = time.Now()
	}

	return utils.WriteJSON(user, handler.GetFile(user.Name))
}

func (handler *UserHandler) Get(userName string) (user *User, err error) {
	user = new(User)
	err = utils.ReadJSON(user, handler.GetFile(userName))
	if err != nil {
		if os.IsNotExist(err) {
			err = NotFound
		}
		user = nil
	}

	return
}

func (handler *UserHandler) Exists(userName string) (exists bool, err error) {
	_, err = os.Stat(handler.GetFile(userName))

	if err == nil {
		exists = true
	} else if os.IsNotExist(err) {
		exists = false
		err = nil
	}

	return
}

func (handler *UserHandler) Delete(userName string) error {
	exists, err := handler.Exists(userName)
	if err != nil {
		return err
	}

	if !exists {
		return NotFound
	}

	return os.Remove(handler.GetFile(userName))
}

func (handler *UserHandler) List() (users []User, err error) {
	users = make([]User, 0)
	userFiles, err := utils.ListDir(handler.GetFolder(), true)
	if err != nil {
		return
	}

	for _, fileName := range userFiles {
		user := new(User)
		err = utils.ReadJSON(user, fileName)
		if err != nil {
			return nil, err
		}

		users = append(users, *user)
	}

	return
}

func (handler *UserHandler) Login(userName, password string) (user *User, err error) {
	user = new(User)
	err = utils.ReadJSON(user, handler.GetFile(userName))

	if err == nil {
		if !utils.EqualPassword(user.Password, utils.CreatePassword(password)) {
			return nil, NotFound
		}
	} else if os.IsNotExist(err) {
		err = nil
		user = nil
	}

	return
}
