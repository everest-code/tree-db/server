=====================
 Configuration Server
=====================

**Tree-DB** doesn't use a configuration file else use flags to configure a change easily the behaviour of the server.

Flags
-----
.. sourcecode:: sh

    tree-db --help
    usage: tree-db [-h|--help] -D|--data-dir "<value>" [-p|--expose "<value>"]
                   [-u|--user] [-v|--verbosity (DEBG|INFO|WARN|ERRO)]
                   [-l|--log-file "<value>"]

                   A simple and powerful Key-Value Tree-Structure Database

    Arguments:
      -h  --help       Print help information
      -D  --data-dir   Database directory
      -p  --expose     host:port to expose the database. Default: :9110
      -u  --user       Allow user authentication for get/set in the database.
                       Default: false
      -v  --verbosity  Log level for debugging and useful data. Default: WARN
      -l  --log-file   Log file if not supplied only will be on stdout


- ``-h | --help``
    Is an optional field how print the help information.

    Usage: ``tree-db --help``
- ``-D | --data-dir``
    Is an required how where define the database path (if not exists will be created by the server).

    Usage: ``tree-db -D "/var/lib/tree-db"``
- ``-p | --expose``
    Is an optional field where set the expose address and port of the server like ``127.0.0.1:9111``, By default is ``:9110`` (expose to all interfaces on port 9110).

    Usage: ``tree-db -D "/var/lib/tree-db" -p 127.0.0.1:9112``
- ``-u | --user``
    Is a flag how allow the user authentication

    .. caution:: If you use this flag but no one user is created the server will be does not grant access to anyone.

    Usage: ``tree-db -D "/var/lib/tree-db" -u``
- ``-v | --verbosity``
    Is an optional field where set the verbose log level, possible options: DEBG, INFO, WARN, ERRO, By default is: ``WARN``.

    Usage: ``tree-db -D "/var/lib/tree-db" -v INFO``

- ``-l | --log-file``
    Is an optional field where set output log path (if not exists will be created by the server).

    .. note:: If log path is not passed will be write the log in STDOUT

    Usage: ``tree-db -D "/var/lib/tree-db" --log-file "/var/log/tree-db.log"``
