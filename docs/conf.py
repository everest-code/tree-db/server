# -*- coding: utf-8 -*-
# fmt:off

source_suffix = ".rst"
master_doc = "index"

# General information about the project.
project = u"Tree-DB"
copyright = u"2022, Everest Code"
author = u"SGT911"
version = u"v0.1.12"

language = "en"
pygments_style = "sphinx"
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "collapse_navigation" : False
}

rst_epilog = """
.. include:: substitutions.txt
"""
