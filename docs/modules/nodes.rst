====================
 API Nodes endpoints
====================

Prefix
------
The prefix of the all routes is ``/n`` referencing to the users namespace,but this is already including the prefix.

.. _nodestructs:

Structs
-------
If some structs are not in this files please refer to `API Structs <index.html#structs>`_ file.

- ``NodeType``
    .. sourcecode:: typescript

        enum NodeType {
            Node,
            Root,
            Bool,
            List,
            Str,
            Number,
            Unknown
        }

- ``Node``
    .. sourcecode:: typescript

        interface Node {
            id: UUID;
            parent?: UUID;
            name: String;
            creationDate: Date;
            type: NodeType;
            value: Any;
        }

- ``NodeCreate``
    .. sourcecode:: typescript

        interface NodeCreate {
            name: String;
            type: NodeType;
        }

Routes
------

.. important:: The request content type is ``application/json`` in the docs is ``text/javascript`` for comments.

- Dump root.
    - Request.
        .. sourcecode:: http

            GET /n/<root> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<Any>
                "ok": true,
                "payload": {
                    // this is an example
                    // the result depends of the tree structure
                    "maxLifes": 3,
                    "cacheTime": 3600,
                    "npc": {
                        "enemyLow": 15,
                        "enemyMedium": 20,
                        "enemyHigh": 30
                    }
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Tree Value.
    - Query Params
        If ``all=yes`` is active will show the node metadata, but if ``tree=yes`` is active and is ``Node`` type will dump the tree.

        .. sourcecode:: http

            GET /n/<root>/<path:"."separated>?all=yes&tree=yes HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==

    - Request.
        .. sourcecode:: http

            GET /n/<root>/<path:"."separated> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<Any>
                "ok": true,
                "payload": {
                    // this is an example
                    // the result depends of the tree structure
                    "enemyLow": 15,
                    "enemyMedium": 20,
                    "enemyHigh": 30
                }
            }

        **Success** ``(all=yes)``

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<Node>
                "ok": true,
                "payload": { // Node
                    "id": "c6df5fec-e431-11eb-af96-c50688e9f027",
                    "name": "npc",
                    "parent": "d5683336-e431-11eb-af96-c50688e9f027",
                    "type": "Node",
                    "value": []
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Create node.
    - Request.

        .. note:: If the name is ``"<auto>"`` will set the name as the ``Node.ID``

        .. sourcecode:: http

            POST /n/<root>(/<path:"."separated>) HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // NodeCreate
                "name": "isServer",
                "type": "Bool"
            }
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 201 CREATED
            Content-Type: text/javascript

            { // ApiResponse<Node>
                "ok": true,
                "payload": { // Node
                    "id": "c6df5fec-e431-11eb-af96-c50688e9f027",
                    "name": "isServer",
                    "parent": "d5683336-e431-11eb-af96-c50688e9f027",
                    "type": "Bool",
                    "value": false
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **BadRequest (Type not valid)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "type is unknown"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }

- Delete node.
    - Request.
        .. sourcecode:: http

            DELETE /n/<root>/<path:"."separated> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==

    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "deleted"
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Update node value.
    - Request.
        .. sourcecode:: http

            PUT /n/<root>/<path:"."separated> HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // SimplePayload
                "value": true
            }
    - Response.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "modified"
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **BadRequest (Value type is not valid)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "The got value is not valid type"
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Get node by ``UUID``.
    - Request.
        .. sourcecode:: http

            GET /n/by-uuid/<id> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<Node>
                "ok": true,
                "payload": { // Node
                    "id": "c6df5fec-e431-11eb-af96-c50688e9f027",
                    "name": "npc",
                    "parent": "d5683336-e431-11eb-af96-c50688e9f027",
                    "type": "Node",
                    "value": []
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }

- Update value by ``UUID``.
    - Request.
        .. sourcecode:: http

            PUT /n/by-uuid/<id> HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // SimplePayload
                "value": true
            }

    - Reponses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "modified"
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **BadRequest (Value type is not valid)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "The got value is not valid type"
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Delete node by ``UUID``.
    - Request.
        .. sourcecode:: http

            DELETE /n/by-uuid/<id> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "deleted"
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Node not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
