===================
 API User endpoints
===================

Prefix
------
The prefix of the all routes is ``/u`` referencing to the users namespace,but this is already including the prefix.

Structs
-------
If some structs are not in this files please refer to `API Structs <index.html#structs>`_ file.

- ``User``
    .. sourcecode:: typescript

        interface User {
            id: UUID;
            name: String;
            createdAt: Date;
            updateDate?: Date;
            writeAllowed: Boolean;
        }
- ``UserCreate``
    .. sourcecode:: typescript

        interface UserCreate {
            user: String;
            password: String;
            writeAllowed?: Boolean;
        }

Routes
------

.. important:: The request content type is ``application/json`` in the docs is ``text/javascript`` for comments.

- List users.
    - Request.
        .. sourcecode:: http

            GET /u/ HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==

    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<[]User>
                "ok": true,
                "payload": [ // []User
                    { // User
                        "id": "17fc8b58-e3ff-11eb-ba80-0242ac130004",
                        "name": "UserA",
                        "createdAt": "2021-07-13T12:27:37.728270-0500",
                        "updateAt": "2021-07-13T12:27:37.728270-0500",
                        "writeAllowed": true
                    },
                    // More users ...
                    { // User
                        "id": "17fc8b59-e3ff-11eb-ba80-0242ac130004",
                        "name": "UserB",
                        "createdAt": "2021-07-13T12:28:59.589277-0500",
                        "updateAt": "2021-07-13T12:28:59.589277-0500",
                        "writeAllowed": false
                    }
                ]
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Create user.
    - Request.
        .. sourcecode:: http

            POST /u/ HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // UserCreate
                "user": "UserC",
                "password": "53cur3 p455w0rd"
            }
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 201 CREATED
            Content-Type: text/javascript

            { // ApiResponse<User>
                "ok": true,
                "payload": { // User
                    "id": "17fc8b59-e3ff-11eb-ba80-0242ac130004",
                    "name": "UserC",
                    "createdAt": "2021-07-13T12:28:59.589277-0500",
                    "updateAt": "2021-07-13T12:28:59.589277-0500",
                    "writeAllowed": false
                }
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **BadRequest (User already exists)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "the user already exists"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }

- Get an user.
    - Request.
        .. sourcecode:: http

            GET /u/<userName> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==

    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<User>
                "ok": true,
                "payload": { // User
                    "id": "17fc8b59-e3ff-11eb-ba80-0242ac130004",
                    "name": "UserC",
                    "createdAt": "2021-07-13T12:28:59.589277-0500",
                    "updateAt": "2021-07-13T12:28:59.589277-0500",
                    "writeAllowed": false
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "User was not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Delete an user.
    - Request.
        .. sourcecode:: http

            DELETE /u/<userName> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "deleted"
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "User was not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Update writing access.
    - Request.
        .. sourcecode:: http

            PUT /u/<userName>/write HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // SimplePayload<bool>
                "value": true
            }
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "modified"
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "User was not found"
                }
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
- Change password.
    - Request.
        .. sourcecode:: http

            PUT /u/<userName>/password HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // SimplePayload<string>
                "value": "53cur3 p455w0rd"
            }
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "modified"
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "User was not found"
                }
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
