===================
 API Documentation
===================

Authentication
--------------
For authentication use `Basic authentication method <https://developer.mozilla.org/en/docs/Web/HTTP/Authentication>`_

.. _structs:

Structs
-------
- ``ApiResponse``
    .. sourcecode:: typescript

        interface ApiResponse<T> {
            ok: Boolean;
            payload: T;
            error?: ApiError;
        }

- ``ApiError``
    .. sourcecode:: typescript

        interface ApiError {
            name: String;
            description?: Any;
        }

- ``SimplePayload``
    .. sourcecode:: typescript

        interface SimplePayload<T> {
            value: T;
        }

Modules
-------
All actions are separated in modules, which are ``users``, ``roots`` and ``nodes``.

|Contents|

.. toctree::
   :maxdepth: 1

   users
   roots
   nodes
