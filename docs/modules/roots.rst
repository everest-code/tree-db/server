===================
 API Root endpoints
===================

Prefix
------
The prefix of the all routes is ``/r`` referencing to the users namespace,but this is already including the prefix.

Structs
-------
If some structs are not in this files please refer to `API Structs <index.html#structs>`_ file, ``Root`` is a child class of ``Nodes`` refers to `Nodes Structs <nodes.html#nodestructs>`_.

- ``RootCreate``
    .. sourcecode:: typescript

        interface RootCreate {
            name: String;
        }

Routes
------

.. important:: The request content type is ``application/json`` in the docs is ``text/javascript`` for comments.

- Create a route.
    - Request.
        .. sourcecode:: http

            POST /r/ HTTP/1.1
            Content-Type: text/javascript
            Authorization: Basic dXNlcjpwYXNzd29yZA==

            { // RootCreate
                "name": "base"
            }

    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 201 CREATED
            Content-Type: text/javascript

            { // ApiResponse<Node>
                "ok": true,
                "payload": { // Node
                    "id": "fc41b316-e2c2-4ad4-aead-b9ed60353ae1",
                    "name": "base",
                    "creationDate": "2021-07-13T09:56:58.569845869-05:00",
                    "type": "Root",
                    "value": []
                }
            }

        **BadRequest (Parse error)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "Parsing error"
                }
            }

        **BadRequest (Root already exists)**

        .. sourcecode:: http

            HTTP/1.1 400 BAD_REQUEST
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "BadRequest",
                    "description": "the root already exists"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }

- List roots.
    - Request.
        .. sourcecode:: http

            GET /r/ HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==

    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<[]Node>
                "ok": true,
                "payload": [ // []Node
                    { // Node
                        "id": "fc41b316-e2c2-4ad4-aead-b9ed60353ae1",
                        "name": "base",
                        "creationDate": "2021-07-13T09:56:58.569845869-05:00",
                        "type": "Root",
                        "value": []
                    },
                    // More users ...
                    { // Node
                        "id": "fc41b32d-e2c2-4ad4-aead-b9ed60353ae1",
                        "name": "users",
                        "creationDate": "2021-07-13T09:56:57.569845869-05:00",
                        "type": "Root",
                        "value": []
                    },
                ]
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }

- Get a root.
    - Request.
        .. sourcecode:: http

            GET /r/<rootName> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Content-Type: text/javascript

            { // ApiResponse<Node>
                "ok": true,
                "payload": { // Node
                    "id": "fc41b316-e2c2-4ad4-aead-b9ed60353ae1",
                    "name": "base",
                    "creationDate": "2021-07-13T09:56:58.569845869-05:00",
                    "type": "Root",
                    "value": []
                }
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Root was not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }

- Delete a root.
    - Request.
        .. sourcecode:: http

            DELETE /r/<rootName> HTTP/1.1
            Authorization: Basic dXNlcjpwYXNzd29yZA==
    - Responses.
        **Success**

        .. sourcecode:: http

            HTTP/1.1 202 ACCEPTED
            Content-Type: text/javascript

            { // ApiResponse<string>
                "ok": true,
                "payload": "deleted"
            }

        **Not Found**

        .. sourcecode:: http

            HTTP/1.1 404 NOT_FOUND
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "NotFound",
                    "description": "Root was not found"
                }
            }

        **Internal Server Error**

        .. sourcecode:: http

            HTTP/1.1 500 INTERNAL_SERVER_ERROR
            Content-Type: text/javascript

            { // ApiResponse
                "ok": false,
                "payload": null,
                "error": { // ApiError
                    "name": "InternalServerError",
                    "description": "error description"
                }
            }
