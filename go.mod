module gitlab.com/everest-code/tree-db/server

go 1.16

require (
	github.com/akamensky/argparse v1.3.1
	github.com/fatih/color v1.12.0
	github.com/google/uuid v1.3.0
	github.com/labstack/echo/v4 v4.5.0
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/crypto v0.0.0-20210915214749-c084706c2272 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
	golang.org/x/sys v0.0.0-20210915083310-ed5796bab164 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
)
