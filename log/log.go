package log

import (
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/fatih/color"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type (
	Level uint8

	Logger struct {
		writer  io.Writer
		Level   Level
		NoColor bool
	}
)

const (
	LevelDebg Level = iota
	LevelInfo
	LevelWarn
	LevelErro
)

func NewConsole(level Level, noColor bool) (logger *Logger, err error) {
	logger = &Logger{
		writer:  os.Stdout,
		Level:   level,
		NoColor: noColor,
	}

	return
}

func NewFile(level Level, fileName string) (logger *Logger, err error) {
	var file *os.File
	var flags int

	flags = os.O_CREATE | os.O_APPEND | os.O_WRONLY
	file, err = os.OpenFile(fileName, flags, 0o644)

	if err == nil {
		logger = &Logger{
			writer:  file,
			Level:   level,
			NoColor: true,
		}
	}

	return
}

func NewConsoleFile(fileName string, level Level, noColor bool) (logger *Logger, err error) {
	logger1, err := NewConsole(level, noColor)
	if err != nil {
		return
	}

	logger2, err := NewFile(level, fileName)
	if err != nil {
		return
	}

	logger, err = NewIO(&utils.ParallelWriter{
		Writers: []io.Writer{logger1, logger2},
	}, level, noColor)

	return
}

func NewIO(io io.Writer, level Level, noColor bool) (logger *Logger, err error) {
	logger = &Logger{
		writer:  io,
		Level:   level,
		NoColor: noColor,
	}

	return
}

func Panic(format string, data ...interface{}) {
	logger, _ := NewConsole(LevelDebg, false)
	logger.Panic(format, data...)
}

func colorize(col color.Attribute, txt string, noColor bool) string {
	if noColor {
		return txt
	}

	return color.New(col).Sprint(txt)
}

func nowString() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func pid() int {
	return os.Getpid()
}

func hostname() string {
	host, err := os.Hostname()
	if err != nil {
		return "<nil>"
	}

	return host
}

func (logger *Logger) Write(p []byte) (int, error) {
	return logger.writer.Write(p)
}

func (logger *Logger) WriteString(data string) {
	logger.Write([]byte(data + "\r\n"))
}

func (logger *Logger) Debug(format string, data ...interface{}) {
	if logger.Level > LevelDebg {
		return
	}

	var prefix string = fmt.Sprintf("[%s | %s](%s | %d)", colorize(color.FgHiGreen, "DEBG", logger.NoColor), nowString(), hostname(), pid())
	var txt string = fmt.Sprintf(format, data...)

	logger.WriteString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) ImportantInfo(format string, data ...interface{}) {
	var prefix string = fmt.Sprintf("[%s | %s](%s | %d)", colorize(color.FgHiBlue, "INFO", logger.NoColor), nowString(), hostname(), pid())
	var txt string = fmt.Sprintf(format, data...)

	logger.WriteString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Info(format string, data ...interface{}) {
	if logger.Level > LevelInfo {
		return
	}

	var prefix string = fmt.Sprintf("[%s | %s](%s | %d)", colorize(color.FgCyan, "INFO", logger.NoColor), nowString(), hostname(), pid())
	var txt string = fmt.Sprintf(format, data...)

	logger.WriteString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Warn(format string, data ...interface{}) {
	if logger.Level > LevelWarn {
		return
	}

	var prefix string = fmt.Sprintf("[%s | %s](%s | %d)", colorize(color.FgYellow, "WARN", logger.NoColor), nowString(), hostname(), pid())
	var txt string = fmt.Sprintf(format, data...)

	logger.WriteString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Error(format string, data ...interface{}) {
	if logger.Level > LevelErro {
		return
	}

	var prefix string = fmt.Sprintf("[%s | %s](%s | %d)", colorize(color.FgRed, "ERR!", logger.NoColor), nowString(), hostname(), pid())
	var txt string = fmt.Sprintf(format, data...)

	logger.WriteString(fmt.Sprintf("%s: %s", prefix, txt))
}

func (logger *Logger) Panic(format string, data ...interface{}) {
	var prefix string = fmt.Sprintf("[%s | %s](%s | %d)", colorize(color.FgRed, "PN!!", logger.NoColor), nowString(), hostname(), pid())
	var txt string = fmt.Sprintf(format, data...)

	panic(errors.New(fmt.Sprintf("%s: %s", prefix, txt)))
}
