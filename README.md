# Tree-DB Server
A HA (High Availability) HTTP, Real Time, Database for Configuration, Key Value and Firebase-Like applications

## Documentation
[Read the documentation here](https://everest-code.gitlab.io/tree-db/server/)

## Donations
[![Support this project!!!](https://github.com/aha999/DonateButtons/raw/master/Paypal.png)](https://www.paypal.com/donate?hosted_button_id=TJPCWB5ABTVXA)
