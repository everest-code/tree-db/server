package main

import (
	"net/http"
	"os"
	"time"

	"github.com/akamensky/argparse"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/everest-code/tree-db/server/db"
	"gitlab.com/everest-code/tree-db/server/handlers"
	"gitlab.com/everest-code/tree-db/server/log"
	"gitlab.com/everest-code/tree-db/server/version"
)

var (
	database *db.Database
	logger   *log.Logger
	Version  = version.Version{1, 0, 0}
)

func parseArgs() (args ConfigArgs) {
	parser := argparse.NewParser("tree-db", "A simple and powerful Key-Value Tree-Structure Database")

	args.DataDir = parser.String("D", "data-dir", &argparse.Options{
		Default:  nil,
		Help:     "Database directory",
		Required: true,
	})

	args.Expose = parser.String("p", "expose", &argparse.Options{
		Default:  ":9110",
		Help:     "host:port to expose the database",
		Required: false,
	})

	args.EnableUser = parser.Flag("u", "user", &argparse.Options{
		Default:  false,
		Help:     "Allow user authentication for get/set in the database",
		Required: false,
	})

	args.LogLevel = parser.Selector("v", "verbosity", []string{
		"DEBG",
		"INFO",
		"WARN",
		"ERRO",
	}, &argparse.Options{
		Default:  "WARN",
		Help:     "Log level for debugging and useful data",
		Required: false,
	})

	args.LogFile = parser.String("l", "log-file", &argparse.Options{
		Default:  nil,
		Help:     "Log file if not supplied only will be on stdout",
		Required: false,
	})

	err := parser.Parse(os.Args)
	if err != nil {
		os.Stdout.Write([]byte(parser.Usage(err)))
		os.Exit(1)
	}

	return
}

func createLogger(args ConfigArgs) (*log.Logger, error) {
	if *args.LogFile != "" {
		return log.NewConsoleFile(*args.LogFile, args.GetLogLevel(), false)
	}

	return log.NewConsole(args.GetLogLevel(), false)
}

func main() {
	args := parseArgs()

	logger, err := createLogger(args)
	if err != nil {
		log.Panic(err.Error())
	}

	logger.ImportantInfo("Running server on address \"%s\"", *args.Expose)
	logger.ImportantInfo("Database path \"%s\"", *args.DataDir)

	serverHandler := echo.New()

	server := &http.Server{
		Addr:         *args.Expose,
		Handler:      serverHandler,
		ReadTimeout:  20 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	database = db.New(logger, *args.DataDir)
	err = database.InitDB()
	if err != nil {
		logger.Panic(err.Error())
	}

	middlewareHandler := &handlers.DefaultHandler{
		Logger: logger,
	}

	userHandler := &handlers.UserHandler{
		Logger:   logger,
		Database: database,
	}

	rootHandler := &handlers.RootHandler{
		Logger:   logger,
		Database: database,
	}

	nodeHandler := &handlers.NodeHandler{
		Logger:   logger,
		Database: database,
	}

	serverHandler.HTTPErrorHandler = middlewareHandler.ErrorHandler
	serverHandler.Use(middlewareHandler.MiddlewarePerformaceLog)
	serverHandler.Use(middlewareHandler.MiddlewareLogRaw)
	serverHandler.Use(middlewareHandler.MiddlewareServerHeaders("Tree-DB", Version))

	serverHandler.GET("/", func(ctx echo.Context) error {
		return ctx.JSONPretty(200, serverHandler.Routes(), handlers.JSONIndent)
	}).Name = "availablesRoutes"

	if *args.EnableUser {
		logger.Warn("User authentication is enabled." +
			" If user is not created, please" +
			" disallow and create an user to allow this option")
		serverHandler.Use(middleware.BasicAuth(userHandler.AuthMiddleware))
	}

	serverHandler.POST("/u/", userHandler.Create).Name = "users.Create"
	serverHandler.GET("/u/", userHandler.List).Name = "users.List"
	serverHandler.GET("/u/:name", userHandler.Get).Name = "users.Get"
	serverHandler.DELETE("/u/:name", userHandler.Delete).Name = "users.Delete"
	serverHandler.PUT("/u/:name/write", userHandler.UpdateWriting).Name = "users.CanWrite"
	serverHandler.PUT("/u/:name/password", userHandler.UpdatePassword).Name = "users.Password"

	serverHandler.POST("/r/", rootHandler.Create).Name = "roots.Create"
	serverHandler.GET("/r/", rootHandler.List).Name = "roots.List"
	serverHandler.GET("/r/:name", rootHandler.Get).Name = "roots.Get"
	serverHandler.DELETE("/r/:name", rootHandler.Delete).Name = "roots.Delete"

	serverHandler.POST("/n/:root", nodeHandler.Create).Name = "nodes.Create"
	serverHandler.POST("/n/:root/:path", nodeHandler.Create).Name = "nodes.CreateWithPath"
	serverHandler.GET("/n/:root", nodeHandler.Dump).Name = "nodes.Dump"
	serverHandler.GET("/n/:root/:path", nodeHandler.Get).Name = "nodes.Get"
	serverHandler.DELETE("/n/:name/:path", nodeHandler.Delete).Name = "nodes.Delete"
	serverHandler.PUT("/n/:name/:path", nodeHandler.Update).Name = "nodes.Update"
	serverHandler.GET("/n/by-uuid/:id", nodeHandler.GetUUID).Name = "nodes.GetUUID"
	serverHandler.PUT("/n/by-uuid/:id", nodeHandler.UpdateUUID).Name = "nodes.UpdateUUID"
	serverHandler.DELETE("/n/by-uuid/:id", nodeHandler.DeleteUUID).Name = "nodes.DeleteUUID"

	server.ListenAndServe()
}
