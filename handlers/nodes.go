package handlers

import (
	"net/http"
	"strings"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/everest-code/tree-db/server/db"
	"gitlab.com/everest-code/tree-db/server/log"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type (
	NodeHandler struct {
		Logger   *log.Logger
		Database *db.Database
	}

	Node struct {
		Name     string      `json:"name"`
		NodeType db.NodeType `json:"type"`
	}
)

func (handler *NodeHandler) Create(ctx echo.Context) error {
	root, path := ctx.Param("root"), strings.Split(ctx.Param("path"), ".")
	if path[0] == "" {
		path = make([]string, 0)
	}

	node, err := handler.Database.Roots().FindFork(root, path)
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s.%s was not found", ctx.Param("root"), ctx.Param("path"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	child := new(Node)
	if err := ctx.Bind(child); err != nil {
		return err
	}

	if (child.Name == "" || !utils.ValidName(child.Name)) && child.Name != "<auto>" {
		return NewHttpError(
			http.StatusBadRequest,
			"name is empty or is not valid",
		)
	} else if child.NodeType == db.TUnknown {
		return NewHttpError(
			http.StatusBadRequest,
			"type is unknown",
		)
	}

	node, err = handler.Database.Nodes().CreateBlank(child.Name, child.NodeType, &node.ID)
	if err != nil {
		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	return ctx.JSONPretty(201, &ApiResponse{
		Ok:      true,
		Payload: node,
		Error:   nil,
	}, JSONIndent)
}

func (handler *NodeHandler) Get(ctx echo.Context) error {
	root, path := ctx.Param("root"), strings.Split(ctx.Param("path"), ".")
	if path[0] == "" {
		path = make([]string, 0)
	}

	node, err := handler.Database.Roots().FindFork(root, path)
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s.%s was not found", ctx.Param("root"), ctx.Param("path"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	handler.Logger.Debug("Node %s(%s) was got from tree search", node.Name, node.ID)

	if ctx.QueryParam("all") == "yes" {
		return ctx.JSONPretty(200, &ApiResponse{
			Ok:      true,
			Payload: node,
			Error:   nil,
		}, JSONIndent)
	}

	if ctx.QueryParam("tree") == "yes" && node.Type == db.TNode {
		_, tree, err := handler.Database.Nodes().Dump(node.ID)
		if err != nil {
			if err == db.NotFound {
				handler.Logger.Warn("Node %s.%s was not found", ctx.Param("root"), ctx.Param("path"))
				return NewHttpError(
					http.StatusNotFound,
					"node was not found",
				)
			}

			handler.Logger.Error("Node get internal error: %s", err)
			return err
		}

		return ctx.JSONPretty(200, &ApiResponse{
			Ok:      true,
			Payload: tree,
			Error:   nil,
		}, JSONIndent)
	}

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: node.Value,
		Error:   nil,
	}, JSONIndent)
}

func (handler *NodeHandler) GetUUID(ctx echo.Context) error {
	nodeId, err := uuid.Parse(ctx.Param("id"))
	if err != nil {
		return NewHttpError(http.StatusBadRequest, err.Error())
	}

	node, err := handler.Database.Nodes().Get(nodeId)
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s was not found", ctx.Param("name"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	handler.Logger.Debug("Node %s(%s) was got", node.Name, node.ID)

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: node,
		Error:   nil,
	}, JSONIndent)
}

func (handler *NodeHandler) UpdateUUID(ctx echo.Context) error {
	nodeId, err := uuid.Parse(ctx.Param("id"))
	if err != nil {
		return ctx.JSONPretty(400, &ApiResponse{
			Ok:      false,
			Payload: nil,
			Error: &ApiError{
				Name:        "BadRequest",
				Description: err.Error(),
			},
		}, JSONIndent)
	}

	node, err := handler.Database.Nodes().Get(nodeId)
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s was not found", ctx.Param("name"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	value := new(SimplePayload)
	if err := ctx.Bind(value); err != nil {
		return err
	}

	if !node.Type.IsValid(value.Payload) {
		return NewHttpError(
			http.StatusBadRequest,
			"the got value is not valid type",
		)
	}

	err = handler.Database.Nodes().UpdateValue(node.ID, value.Payload)
	if err != nil {
		return err
	}

	handler.Logger.Debug("Node %s(%s) was updated", node.Name, node.ID)

	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: "modified",
		Error:   nil,
	}, JSONIndent)
}

func (handler *NodeHandler) Dump(ctx echo.Context) error {
	dump, err := handler.Database.Roots().DumpTree(ctx.Param("root"))
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Root %s was not found", ctx.Param("root"))
			return NewHttpError(
				http.StatusNotFound,
				"root was not found",
			)
		}

		handler.Logger.Error("Root get internal error: %s", err)
		return err
	}

	handler.Logger.Debug("Root %s was dumpped", ctx.Param("root"))

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: dump,
		Error:   nil,
	}, JSONIndent)
}

func (handler *NodeHandler) DeleteUUID(ctx echo.Context) error {
	nodeId, err := uuid.Parse(ctx.Param("id"))
	if err != nil {
		return NewHttpError(http.StatusBadRequest, err.Error())
	}

	node, err := handler.Database.Nodes().Get(nodeId)
	if err != nil {
		handler.Logger.Error("Node get internal error on delete: %s", err)
		return err
	}
	if node.Type == db.TRoot {
		err = handler.Database.Roots().Delete(node.Name)
	} else {
		err = handler.Database.Nodes().Delete(nodeId)
	}

	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s was not found", ctx.Param("id"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error on delete: %s", err)
		return err
	}

	handler.Logger.Debug("Node %s was deleted", ctx.Param("id"))
	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: "deleted",
	}, JSONIndent)
}

func (handler *NodeHandler) Delete(ctx echo.Context) error {
	root, path := ctx.Param("root"), strings.Split(ctx.Param("path"), ".")
	if path[0] == "" {
		path = make([]string, 0)
	}

	node, err := handler.Database.Roots().FindFork(root, path)
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s.%s was not found", ctx.Param("root"), ctx.Param("path"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	if node.Type == db.TRoot {
		err = handler.Database.Roots().Delete(node.Name)
	} else {
		err = handler.Database.Nodes().Delete(node.ID)
	}
	if err != nil {
		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	handler.Logger.Debug("Node %s(%s) was deleted", node.Name, node.ID)
	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: "deleted",
	}, JSONIndent)
}

func (handler *NodeHandler) Update(ctx echo.Context) error {
	root, path := ctx.Param("root"), strings.Split(ctx.Param("path"), ".")
	if path[0] == "" {
		path = make([]string, 0)
	}

	node, err := handler.Database.Roots().FindFork(root, path)
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Node %s.%s was not found", ctx.Param("root"), ctx.Param("path"))
			return NewHttpError(
				http.StatusNotFound,
				"node was not found",
			)
		}

		handler.Logger.Error("Node get internal error: %s", err)
		return err
	}

	value := new(SimplePayload)
	if err := ctx.Bind(value); err != nil {
		return err
	}

	handler.Logger.Debug("Got value %#v on id %s", value.Payload, node.ID)
	if !node.Type.IsValid(value.Payload) {
		return NewHttpError(
			http.StatusBadRequest,
			"the got value is not valid type",
		)
	}

	err = handler.Database.Nodes().UpdateValue(node.ID, value.Payload)
	if err != nil {
		return err
	}

	handler.Logger.Debug("Node %s(%s) was updated", node.Name, node.ID)

	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: "modified",
		Error:   nil,
	}, JSONIndent)
}
