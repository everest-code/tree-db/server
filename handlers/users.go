package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/everest-code/tree-db/server/db"
	"gitlab.com/everest-code/tree-db/server/log"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type UserHandler struct {
	Logger   *log.Logger
	Database *db.Database
}

type User struct {
	UserName string `json:"user"`
	Password string `json:"password"`
	CanWrite bool   `json:"writeAllowed,omitempty"`
}

func (handler *UserHandler) List(ctx echo.Context) error {
	users, err := handler.Database.Users().List()
	if err != nil {
		handler.Logger.Error("Error on user listing: %s", err.Error())
		return err
	}

	handler.Logger.Debug("%d user(s) listed", len(users))

	for k, _ := range users {
		users[k].Password = nil
	}

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: users,
		Error:   nil,
	}, JSONIndent)
}

func (handler *UserHandler) Get(ctx echo.Context) error {
	user, err := handler.Database.Users().Get(ctx.Param("name"))
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("User %s was not found", ctx.Param("name"))
			return NewHttpError(
				http.StatusNotFound,
				"user was not found",
			)
		}

		handler.Logger.Error("User get internal error: %s", err)
		return err
	}

	user.Password = nil
	handler.Logger.Debug("User %s was got", user.Name)

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: user,
		Error:   nil,
	}, JSONIndent)
}

func (handler *UserHandler) Create(ctx echo.Context) error {
	user := new(User)
	if err := ctx.Bind(user); err != nil {
		return err
	}

	if user.UserName == "" || user.Password == "" {
		return NewHttpError(
			http.StatusBadRequest,
			"user & password are required fields",
		)
	} else if !utils.ValidName(user.UserName) {
		return NewHttpError(
			http.StatusBadRequest,
			"the username is not valid",
		)
	}

	if exist, err := handler.Database.Users().Exists(user.UserName); err != nil {
		return err
	} else if exist {
		handler.Logger.Warn("Trying create an existent user")
		return NewHttpError(
			http.StatusBadRequest,
			"the user already exists",
		)
	}

	dbUser := &db.User{
		Name:     user.UserName,
		Password: utils.CreatePassword(user.Password),
		CanWrite: user.CanWrite,
	}

	err := handler.Database.Users().CreateOrUpdate(dbUser)
	if err != nil {
		handler.Logger.Error("Error creating user: %s", err)
		return err
	}

	dbUser.Password = nil
	handler.Logger.Info("User %s was created", user.UserName)

	return ctx.JSONPretty(201, &ApiResponse{
		Ok:      true,
		Payload: dbUser,
		Error:   nil,
	}, JSONIndent)
}

func (handler *UserHandler) UpdateWriting(ctx echo.Context) error {
	param := new(SimplePayload)
	if err := ctx.Bind(param); err != nil {
		return err
	}

	val, ok := param.Payload.(bool)
	if !ok {
		return NewHttpError(
			http.StatusBadRequest,
			"the value must be a bool",
		)
	}

	userName := ctx.Param("name")

	if exist, err := handler.Database.Users().Exists(userName); err != nil {
		return err
	} else if !exist {
		handler.Logger.Warn("User %s was not found", ctx.Param("name"))
		return NewHttpError(
			http.StatusNotFound,
			"the user was not found",
		)
	}

	user, err := handler.Database.Users().Get(userName)
	if err != nil {
		return err
	}

	user.CanWrite = val

	err = handler.Database.Users().CreateOrUpdate(user)
	if err != nil {
		return err
	}

	handler.Logger.Info("User %s change permission to %s", user.Name, user.Permission())

	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: nil,
		Error:   nil,
	}, JSONIndent)
}

func (handler *UserHandler) UpdatePassword(ctx echo.Context) error {
	param := new(SimplePayload)
	if err := ctx.Bind(param); err != nil {
		return err
	}

	val, ok := param.Payload.(string)
	if !ok || val == "" {
		return NewHttpError(
			http.StatusBadRequest,
			"the value must be a string",
		)
	}

	userName := ctx.Param("name")

	if exist, err := handler.Database.Users().Exists(userName); err != nil {
		return err
	} else if !exist {
		handler.Logger.Warn("User %s was not found", ctx.Param("name"))
		return NewHttpError(
			http.StatusNotFound,
			"the user was not found",
		)
	}

	user, err := handler.Database.Users().Get(userName)
	if err != nil {
		return err
	}

	user.Password = utils.CreatePassword(val)

	err = handler.Database.Users().CreateOrUpdate(user)
	if err != nil {
		return err
	}

	handler.Logger.Info("User %s change password", user.Name)

	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: nil,
		Error:   nil,
	}, JSONIndent)
}

func (handler *UserHandler) Delete(ctx echo.Context) error {
	userName := ctx.Param("name")
	exist, err := handler.Database.Users().Exists(userName)
	if err != nil {
		return err
	}

	if !exist {
		handler.Logger.Warn("User %s was not found", ctx.Param("name"))
		return NewHttpError(
			http.StatusNotFound,
			"the user was not found",
		)
	}

	if err := handler.Database.Users().Delete(userName); err != nil {
		return err
	}

	handler.Logger.Info("User %s was deleted", userName)

	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: nil,
		Error:   nil,
	}, JSONIndent)
}

func (handler *UserHandler) AuthMiddleware(userName, password string, ctx echo.Context) (bool, error) {
	handler.Logger.Debug("Auth data User=\"%s\", Password=\"%s\"", userName, password)
	user, err := handler.Database.Users().Login(userName, password)
	if err != nil {
		if err == db.NotFound {
			return false, nil
		}

		return false, err
	}

	if user == nil {
		return false, nil
	}

	if ctx.Request().Method != "GET" && !user.CanWrite {
		return false, NewHttpError(
			http.StatusForbidden,
			"the user is read-only",
		)
	}

	return true, nil
}
