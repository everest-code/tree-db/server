package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/everest-code/tree-db/server/db"
	"gitlab.com/everest-code/tree-db/server/log"
	"gitlab.com/everest-code/tree-db/server/utils"
)

type RootHandler struct {
	Logger   *log.Logger
	Database *db.Database
}

type Root struct {
	Name string `json:"name"`
}

func (handler *RootHandler) Create(ctx echo.Context) error {
	root := new(Root)
	if err := ctx.Bind(root); err != nil {
		return err
	}

	if root.Name == "" || !utils.ValidName(root.Name) {
		return NewHttpError(
			http.StatusBadRequest,
			"name is empty or is not valid",
		)
	}

	if exist, err := handler.Database.Roots().Exists(root.Name); err != nil {
		return err
	} else if exist {
		handler.Logger.Warn("Trying create an existent root")
		return NewHttpError(
			http.StatusForbidden,
			"the root already exists",
		)
	}

	dbRoot, err := handler.Database.Roots().Create(root.Name)
	if err != nil {
		handler.Logger.Error("Error creating root: %s", err)
		return err
	}

	handler.Logger.Info("Root %s(%s) was created", dbRoot.Name, dbRoot.ID)

	return ctx.JSONPretty(201, &ApiResponse{
		Ok:      true,
		Payload: dbRoot,
		Error:   nil,
	}, JSONIndent)
}

func (handler *RootHandler) Get(ctx echo.Context) error {
	root, err := handler.Database.Roots().Get(ctx.Param("name"))
	if err != nil {
		if err == db.NotFound {
			handler.Logger.Warn("Root %s was not found", ctx.Param("name"))
			return NewHttpError(
				http.StatusNotFound,
				"root was not found",
			)
		}

		handler.Logger.Error("Root get internal error: %s", err)
		return err
	}

	handler.Logger.Debug("Root %s(%s) was got", root.Name, root.ID)

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: root,
		Error:   nil,
	}, JSONIndent)
}

func (handler *RootHandler) List(ctx echo.Context) error {
	roots, err := handler.Database.Roots().List()
	if err != nil {
		handler.Logger.Error("Error on roots listing: %s", err.Error())
		return err
	}

	return ctx.JSONPretty(200, &ApiResponse{
		Ok:      true,
		Payload: roots,
		Error:   nil,
	}, JSONIndent)
}

func (handler *RootHandler) Delete(ctx echo.Context) error {
	name := ctx.Param("name")
	exist, err := handler.Database.Roots().Exists(name)
	if err != nil {
		return err
	}

	if !exist {
		handler.Logger.Warn("Root %s was not found", ctx.Param("name"))
		return NewHttpError(
			http.StatusNotFound,
			"the root was not found",
		)
	}

	if err := handler.Database.Roots().Delete(name); err != nil {
		return err
	}

	handler.Logger.Info("Root %s was deleted", name)

	return ctx.JSONPretty(202, &ApiResponse{
		Ok:      true,
		Payload: nil,
		Error:   nil,
	}, JSONIndent)
}
