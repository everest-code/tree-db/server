package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/everest-code/tree-db/server/log"
	"gitlab.com/everest-code/tree-db/server/version"
)

const JSONIndent string = "    "

type DefaultHandler struct {
	Logger *log.Logger
}

func (handler *DefaultHandler) ErrorHandler(err error, ctx echo.Context) {
	handler.Logger.Error("Error \"%s\" was raised on route %s", err, ctx.Request().URL)
	handler.Logger.Debug("Instance = %#v, Data = %s", err, err)

	var httpErr *echo.HTTPError
	var ok bool
	if httpErr, ok = err.(*echo.HTTPError); !ok {
		if wrappedErr, ok := err.(ApiErrorWrapper); ok {
			err = ctx.JSONPretty(wrappedErr.Status(), ApiResponse{
				Ok:      false,
				Payload: nil,
				Error:   wrappedErr.ApiError(),
			}, JSONIndent)
		} else {
			err = ctx.JSONPretty(http.StatusInternalServerError, ApiResponse{
				Ok:      false,
				Payload: nil,
				Error: &ApiError{
					Name:        http.StatusText(http.StatusInternalServerError),
					Description: err.Error(),
				},
			}, JSONIndent)
		}
		return
	}

	errName := http.StatusText(httpErr.Code)
	var description interface{} = httpErr.Message
	if httpErr.Internal != nil {
		description = httpErr.Internal.Error()
	}

	if errName == description {
		description = nil
	}

	ctx.JSONPretty(httpErr.Code, ApiResponse{
		Ok:      false,
		Payload: nil,
		Error: &ApiError{
			Name:        errName,
			Description: description,
		},
	}, JSONIndent)
}

func (handler *DefaultHandler) MiddlewareServerHeaders(prog string, ver version.Version) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			ctx.Response().Header().Add("Server", fmt.Sprintf("%s/%s", prog, ver.String()))
			return next(ctx)
		}
	}
}

func (handler *DefaultHandler) MiddlewareLogRaw(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		method, url := ctx.Request().Method, ctx.Request().URL
		ip, userAgent := ctx.RealIP(), ctx.Request().Header.Get("User-Agent")

		handler.Logger.Info("Request=\"%s %s\" IP=\"%s\" UserAgent=\"%s\"",
			method, url, ip, userAgent)

		return next(ctx)
	}
}

func (handler *DefaultHandler) MiddlewarePerformaceLog(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		start := time.Now()
		err = next(ctx)
		diff := time.Now().Sub(start)

		method, url := ctx.Request().Method, ctx.Request().URL
		handler.Logger.Debug("Performance of request \"%s %s\" => %s", method, url, diff.String())

		return
	}
}
