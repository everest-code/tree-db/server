package handlers

import (
	"fmt"
	"net/http"
)

type (
	ApiErrorWrapper interface {
		Status() int
		Error() string
		ApiError() *ApiError
	}

	HttpErrorWrapper struct {
		status      int
		description interface{}
	}

	CustomErrorWrapper struct {
		status      int
		name        string
		description interface{}
	}

	ApiError struct {
		Name        string      `json:"name"`
		Description interface{} `json:"error,omitempty"`
	}

	ApiResponse struct {
		Ok      bool        `json:"ok"`
		Payload interface{} `json:"payload"`
		Error   *ApiError   `json:"error,omitempty"`
	}

	SimplePayload struct {
		Payload interface{} `json:"value"`
	}
)

func (err *ApiError) Error() string {
	if err.Description == nil {
		return err.Name
	}

	return fmt.Sprintf("[%s]: %v", err.Name, err.Description)
}

func NewCustomError(status int, name string, description interface{}) *CustomErrorWrapper {
	return &CustomErrorWrapper{status, name, description}
}

func (err *CustomErrorWrapper) Status() int {
	return err.status
}

func (err *CustomErrorWrapper) ApiError() *ApiError {
	return &ApiError{
		Name:        err.name,
		Description: err.description,
	}
}

func (err *CustomErrorWrapper) Error() string {
	return err.ApiError().Error()
}

func (err *HttpErrorWrapper) Status() int {
	return err.status
}

func NewHttpError(status int, description interface{}) *HttpErrorWrapper {
	return &HttpErrorWrapper{status, description}
}

func (err *HttpErrorWrapper) ApiError() *ApiError {
	return &ApiError{
		Name:        http.StatusText(err.status),
		Description: err.description,
	}
}

func (err *HttpErrorWrapper) Error() string {
	return err.ApiError().Error()
}
