package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

func ListDir(path string, onlyFiles bool) (paths []string, err error) {
	paths = make([]string, 0)
	rawPaths, err := ioutil.ReadDir(path)
	if err != nil {
		return
	}

	for _, file := range rawPaths {
		if onlyFiles && file.IsDir() {
			continue
		}

		paths = append(paths, strings.Join([]string{path, file.Name()}, string(os.PathSeparator)))
	}

	return
}

func WriteJSON(data interface{}, fileName string) (err error) {
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o0644)
	if err != nil {
		return err
	}
	defer func() {
		err2 := file.Close()
		if err == nil {
			err = err2
		}
	}()

	body, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = file.Write(body)
	if err != nil {
		return err
	}

	return nil
}

func ReadJSON(obj interface{}, fileName string) (err error) {
	file, err := os.OpenFile(fileName, os.O_RDONLY, 0o0644)
	if err != nil {
		return
	}
	defer func() {
		err2 := file.Close()
		if err == nil {
			err = err2
		}
	}()

	body, err := ioutil.ReadAll(file)
	if err != nil {
		return
	}

	return json.Unmarshal(body, obj)
}
