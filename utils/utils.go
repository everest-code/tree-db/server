package utils

import (
	"errors"

	"github.com/google/uuid"
)

func ToUUIDArray(data interface{}) ([]uuid.UUID, error) {
	arrRaw, ok := data.([]interface{})
	resArr := make([]uuid.UUID, 0)
	if !ok {
		return nil, errors.New("Cant parse to array")
	}

	for _, val := range arrRaw {
		id, err := uuid.Parse(val.(string))
		if err != nil {
			return nil, err
		}

		resArr = append(resArr, id)
	}
	return resArr, nil
}
