package utils

import (
	crypt "crypto/sha512"
)

func CreatePassword(password string) []byte {
	hash := crypt.New()
	hash.Write([]byte(password))

	return hash.Sum(nil)
}

func EqualPassword(arr1, arr2 []byte) bool {
	if len(arr1) != len(arr2) {
		return false
	}

	for k, v := range arr1 {
		if v != arr2[k] {
			return false
		}
	}

	return true
}
