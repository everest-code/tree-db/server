package utils

import "regexp"

func ValidName(name string) bool {
	re := regexp.MustCompile("^[a-z0-9][a-z0-9_\\-]{2,14}[a-z0-9]$")
	return re.Match([]byte(name))
}
